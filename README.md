Role Name
=========

Ansible role to install LibreNMS (http://www.librenms.org/). 

Requirements
------------


Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

This role relies on other roles to install and configure LibreNMS's pre-requisites:

* geerlingguy.nginx (https://galaxy.ansible.com/geerlingguy/nginx/)
* geerlingguy.mysql (https://galaxy.ansible.com/geerlingguy/mysql/)
* geerlingguy.repo-epel (https://galaxy.ansible.com/geerlingguy/repo-epel/)
* geerlingguy.memcached (https://galaxy.ansible.com/geerlingguy/memcached/)

Check the example playbook below on how to customise these dependencies.


Example Playbook
----------------

The example playbook below installs LibreNMS and dependencies on CentOS 7. 

  - hosts: localhost
    become: yes
    roles:
      - { role: geerlingguy.repo-epel }
      - { role: geerlingguy.nginx }
      - { role: geerlingguy.mysql }
      - { role: geerlingguy.memcached }
    vars:
      nginx_remove_default_vhost: true
      # Necessary adjustments for Mariadb on CentOS 7
      mysql_packages:
        - mariadb
        - mariadb-server
        - mariadb-libs
        - MySQL-python
        - perl-DBD-MySQL
      mysql_daemon: mariadb
      mysql_log_error: /var/log/mariadb/mariadb.log
      mysql_syslog_tag: mariadb
      mysql_pid_file: /var/run/mariadb/mariadb.pid
      # Create observium database and user.
      mysql_databases:
        - name: observium
      mysql_users:
        - name: observium
          host: 127.0.0.1
          password: observium
          priv: observium.*:USAGE
      mysql_enablerepo: "epel"
      mysql_bind_address: "127.0.0.1"

    

License
-------

BSD

Author Information
------------------

Markus Juenemann, 2016.

This Ansible role is loosely based on https://galaxy.ansible.com/sfromm/librenms/. 
